var subformCache = {};

var SubForm = function(config){
    this.config = config;
    
    //keep track of all subforms on the page to allow for related form option addition
    subformCache[this.config.form_id] = this;

};

SubForm.prototype.addOption = function(item, selected){
 
    var i;
    
    wrapper = dojo.query('#'+this.config.form_id+'_wrapper')[0];
    
    pk_names = this.config.pk_names;
    display_field_name = this.config.display_field_name;
    
    pk_values = [];
    
    for(i=0; i<pk_names.length; i++){
        pk_values.push(item[pk_names[i]]);
    }
    
    value = pk_values.join('/');
    name = item[display_field_name];
    
    //find the select field that share's the wrapper's (great-)*grandparent
    selectfield = wrapper.parentNode.parentNode.children[0];
    
    var option = dojo.create('option');
    option.value = value;
    option.innerHTML = name;
    option.selected = selected;
    selectfield.options.add(option, 0);
};

SubForm.prototype.updateRelatedSubForms = function(item){
    var i;

    relatedSubForms = this.config.related_subforms;

    for (i=0; i<relatedSubForms.length; i++){
        subform = subformCache[relatedSubForms[i]];
        if (subform !== this){
            subform.addOption(item, false);
        }
    }
};

SubForm.prototype.clearValues = function(){
    var field_name;
    fields = this.getFields();
    for (field_name in fields){
        field = fields[field_name];
        field.value = '';
    }
};

SubForm.prototype.submit = function(){
    var config = this.config;
    
    var url = "./";
    if ((config!==undefined)&&(config.url !== undefined)){
        url = config.url;
    }
    var values = this.getValues();
    
    values['__subtype'] = config.subtype;
    values['_method'] = 'subtype_post';
    
    var subform = this;

    dojo.xhrPost( {
        // The following URL must match that used to test the server.
        url: url, 
        handleAs: "json",
        content: values,
        timeout: 5000, // Time in milliseconds
        preventCache: true,

        // The LOAD function will be called on a successful response.
        load: function(response, ioArgs) { 
            s = subform;
            subform.clearValues();
            toggleSubForm(subform.config.form_id);
            subform.addOption(response, true);
            subform.updateRelatedSubForms(response);
        },

        /*
        //The ERROR function will be called in an error case.
        error: function(response, ioArgs) { 
            console.error("HTTP status code: ", ioArgs.xhr.status);
            return false;
        }*/
    });

};

SubForm.prototype.getFields = function(){
    var field;
    var form_id = this.config.form_id;
    var subform = dojo.query('div[id="'+form_id+'_wrapper"]')[0];

    var fields = {};
    
    var subfields = subform.getElementsByTagName('input');
    for (i=0; i<subfields.length; i++){
        field = subfields[i];
        if (field.name !=''){
            fields[field.name] = field;
        }
    }
    return fields;
};

SubForm.prototype.getValues = function(){
    var fieldValues = {};
    var fields = this.getFields();
    var field_name, field;
    
    for (field_name in fields){
        field = fields[field_name];
        if (field.name !=''){
            fields[field.name] = field;
            fieldValues[field.name] = field.value;
        }
    }
    return fieldValues;
};

SubForm.prototype.validate = function(){
    var config = this.config;
    var i;
    var url = "./";
    if ((config!==undefined)&&(config.url !== undefined)){
        url = config.url;
    }
    var form_id = config.form_id;
    var fieldValues = this.getValues();
    var fields = this.getFields();

    var subform = this;
    
    fieldValues['_method'] = 'validate';
    fieldValues['__subtype'] = config.subtype;

    dojo.xhrGet( {
        // The following URL must match that used to test the server.
        url: url, 
        handleAs: "json",
        // form: form_id,
        content: fieldValues,
        timeout: 5000, // Time in milliseconds
        preventCache: true,

        // The LOAD function will be called on a successful response.
        load: function(response, ioArgs) { 
            var errors = response;
            for (var key in fields){
                if (fields.hasOwnProperty(key)){
                    var error = '';
                    if (errors.hasOwnProperty(key)){
                        error = errors[key];
                    }
                    var field = fields[key];
                    var input_id = form_id+'_'+key;
                    var error_span_id = input_id+'_error';
                    var el = dojo.byId(error_span_id);
                    if (el === null){
                        //create the span if it doesn't exist
                        el = dojo.create('span', {'id':error_span_id, 'class':'fielderror'});
                        field.parentNode.appendChild(el);
                    }
                    //clear any existing errors
                    if ((typeof error == "undefined")&&(typeof el != "undefined")&&(el !== null)){
                        el.innerHTML = '';
                        continue;
                    }
                    //mark any errors
                    if ((typeof el !== "undefined")&&(el !== null)){
                        el.innerHTML = error;
                    }
                }
                
            }
            //there are no errors
            if (Object.keys(errors).length === 0){
                subform.submit();
            }
            return false;
        },

        /*
        // The ERROR function will be called in an error case.
        error: function(response, ioArgs) { 
            console.error("HTTP status code: ", ioArgs.xhr.status);
            //return response;
            return false;
        }*/
    });
    return false;
};

var toggleSubForm = function(id){
    var i,c;
    var q = dojo.query('#'+id+'_opener');
    if (q.length < 1){
        return;
    }
    var el = q[0];
    var wrapper = dojo.query('#'+id+'_wrapper')[0];
    

    for (i=0; i<el.classList.length; i++){
        c = el.classList[i];
        if (c == 'subform-opener-open'){
            q.removeClass('subform-opener-open');
            q.addClass('subform-opener-closed');
            wrapper.style.display='none';
            return;
        }
    }
    q.removeClass('subform-opener-closed');
    q.addClass('subform-opener-open');
    wrapper.style.display='block';

};