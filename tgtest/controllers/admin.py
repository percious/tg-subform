from tgext.admin.tgadminconfig import TGAdminConfig
from tgext.admin.config import CrudRestControllerConfig
from tgext.subform.config import SubformController
from tgext.subform.widgets import SelectFieldWithAdd

from tw.forms import TextField
from tgtest.model import User

from sprox.dojo.formbase import DojoAddRecordForm

class TownSelectField(SelectFieldWithAdd):
    __field_widget_types__ = dict(name=TextField)

    #xxx make this automatic
    related_subforms = ['town', 'town2']
    
class UserAddForm(DojoAddRecordForm):
    __entity__ = User
 
    town = TownSelectField
    town2 = TownSelectField
    email_address=TextField
    display_name=TextField

class UserConfig(CrudRestControllerConfig):
    
    defaultCrudRestController=SubformController
    new_form_type=UserAddForm

class TgTestAdminConfig(TGAdminConfig):
    
    user=UserConfig
    
    